
import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
declare const Notes: any;
@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements AfterViewInit {

  public isChecked: boolean = false;
  editor: any;
  storedItem: any = localStorage.getItem("userInput");
  @ViewChild('editable', { static: true })
  editable!: ElementRef;
  constructor() { }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '130px',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Ayer tuve pendiente...",
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
    ],
    customClasses: [
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    sanitize: true,

    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'underline',
        'strikeThrough',
        'subscript',
        'superscript',
        'justifyRight',
        'justifyFull',
        'indent',
        'outdent',
        'heading',
        'fontName'
      ],
      [
        'fontSize',
        'textColor',
        'backgroundColor',
        'customClasses',
        'insertVideo',
        'insertImage',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]

    ]
  };

  ngAfterViewInit() {
    this.editor = new Notes(this.editable.nativeElement);
    this.editor.subscribe('editableInput', (eventObj: { target: { innerHTML: any; }; }) => {
      let userInput = eventObj.target.innerHTML;
      this.storedItem = localStorage.getItem("userInput");
      if (this.storedItem) {
        localStorage.removeItem("userInput");
        localStorage.setItem("userInput", userInput);
      } else {
        this.storedItem = localStorage.getItem("userInput");
        localStorage.setItem("userInput", userInput);
      }
    });

    this.loadContent();
  }

  loadContent() {
    const storedTextContent = this.getTextFromStored(this.storedItem);
    if (storedTextContent.length > 0) {
      this.editable.nativeElement.click();
      this.editable.nativeElement.innerHTML = this.storedItem;
    }
  }

  exportData() {
    const editorContent = this.editor.getContent();
    if (editorContent) {
      this.exportNotes(editorContent);
    } else {
      const storedContent = localStorage.getItem("userInput");
      this.exportNotes(storedContent);
    }
  }

  generateFileName() {
    const today = new Date();
    const todayToISO = today.toISOString();
    return 'Browser_notes_' + todayToISO + '_notes.html';
  }

  exportNotes(editorContent: string | number | boolean | null) {
    let anchor = document.querySelector("a");
    anchor.setAttribute('download', this.generateFileName());
    anchor.setAttribute('href', 'data:text/html;charset=UTF-8,' + encodeURIComponent(editorContent));
  }

  getTextFromStored(storedContent: string) {
    const parser = new DOMParser();
    let output = parser.parseFromString(storedContent, 'text/html');
    return output.body.innerText;
  }

}
